#include <stdio.h>

#define N 8

int main(void){
	
	int v1[N]={3,-3,5,6,2,4,2,11},v2[N]={5,-6,7,6,2,4,2,9};
	int temp1=0,temp2=0;
	
	printf("VETTORE 1\n");
	
	for(int i=0; v1[i]<=(N-1);i=v1[i]){
		
		temp1++;
		
		if (temp1>=N)
			break;
		
	}
	
	if(temp1>=N-1)
		printf("VETT 1 e' ciclico\n");
	else {
		printf("VETT 1 NON e' ciclico\n");
	}
	
	printf("\nVETTORE 2\n");
	
	for(int j=0; v2[j]<=(N-1);j=v2[j]){
		
		temp2++;
		
		if (temp2>=N)
			break;
		
	}
	
	if(temp2>=N-1)
		printf("VETT 2 e' ciclico\n");
	else {
		printf("VETT 2 NON e' ciclico\n");
	}
	

	
}