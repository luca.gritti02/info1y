/*

Decripta il messaggio criptato tramite l'esercizio FI20160222-esercizio2.c

*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    int k;
    char c;
    printf("Inserisci la chiave di decriptazione: ");
    scanf("%d", &k);

    FILE *input, *output;
    input = fopen("codice.txt", "r");
    output = fopen("mesaggioDecrypt.txt", "w");

    while((c = getc(input)) != EOF)
        putc((c-k), output);

    fclose(output);
    fclose(input);

    return 0;
}
