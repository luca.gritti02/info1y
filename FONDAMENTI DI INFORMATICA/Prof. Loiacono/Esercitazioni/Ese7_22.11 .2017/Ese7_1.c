/*

Scrivere in C una funzione void fun(FILE *f1, FILE *f2) che prende come parametri due puntatori a file; f2 è un puntatore ad un file testuale (aperto in scrittura),
f1 è un puntatore a un file binario (aperto in lettura) che contiene una sequenza di coppie di valori interi v1 e v2; v1 rappresenta la codifica ASCII del carattere
da scrivere in f2 e v2 il numero di volte che l'operazione deve essere ripetuta, ovvero quanti caratteri v1 devono essere scritti in f2.
Le diverse sequenze di v2 caratteri v1 devono essere separate tra loro da uno spazio.

*/

#include <stdio.h>

void fun(FILE *f1, FILE *f2);

int main() {

    FILE *out, *in;
    in = fopen("in.dat", "wb");
    int coppie[] = {'a',3,'b',2,'c',1};
    fwrite(coppie, sizeof(int), 6, in);
    fclose(in);

    in = fopen("in.dat", "rb");
    out = fopen("out.txt", "wb");

    fun(in, out);

    fclose(in);
    fclose(out);

    return 0;

}

void fun(FILE *f1, FILE *f2) {

    int v1, v2;

    while(fread(&v1, sizeof(int), 1, f1) != 0) { //legge il primo (nel caso del primo ciclo) intero del file binario. Lo metto nel while perchè il ciclo va fatto finchè fread ritorna !=0. Se ritorna 0 vuole dire che non è stato letto e quindi si è arrivati alla fine del fine
    fread(&v2, sizeof(int), 1, f1); //legge l'intero dopo a quello letto sopra del file banario (metto sempre 1 perchè il cursore si sposta man mano che si legge)

    for(int i=0; i < v2; i++) { //scrivo il carattere v1 v2 volte
        fprintf(f2, "%c", v1);
    }

    fprintf(f2, " "); //aggiungo lo spazio

    }
}
