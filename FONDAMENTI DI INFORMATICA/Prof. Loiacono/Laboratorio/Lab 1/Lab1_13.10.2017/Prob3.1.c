#include <stdio.h>

int main()
{
    int num1, num2, num3, max, min;

    /* Input three numbers from user */
    printf("Inserire 3 numeri interni: ");
    scanf("%d%d%d%*c", &num1, &num2, &num3);

    // ***TROVO IL MASSIMO***
    if((num1 > num2) && (num1 > num3))
        max = num1;
    else if(num2 > num3) //so già che num1 non è il massimo perchè altrimenti si sarebbe fermato prima. Quindi controllo se num2 è il max
        max = num2;
    else //se num1 e num2 non sono il massimo, lo è num3 per forza
        max = num3;

    // ***TROVO IL MINIMO***
    if((num1 < num2) && (num1 < num3))
        min = num1;
    else if(num2 < num3) //so già che num1 non è il minimo perchè altrimenti si sarebbe fermato prima. Quindi controllo se num2 è il min
        min = num2;
    else //se num1 e num2 non sono il minimo, lo è num3 per forza
        min = num3;

    printf("MAX = %d\nMIN = %d", max, min);

    return 0;
}
